import { DELETE_TASK, ADD_TASK} from '../constants/actionTypes';

export const createTask = (value) => {
  return {
    type: ADD_TASK,
    value
  }
};

export const deleteTask = (value) => {
  return {
    type: DELETE_TASK,
    value
  }
};

