import { createAppContainer } from "react-navigation";
import { createBottomTabNavigator } from 'react-navigation-tabs'

import TaskManagerNavigation from "./MainScreenSwitch"

import MessagesScreen from '../src/screens/MessagesScreen/MessagesScreen';
import MapsScreen from '../src/screens/MapsScreen/MapsScreen';


const AppNavigator = createBottomTabNavigator({
    Screen: {
      screen: TaskManagerNavigation,
      navigationOptions: {
          title: "Task manager"
      }
    },
    Maps: {
        screen: MapsScreen,
        navigationOptions: {
            title: "Maps"
        } },
    Messages: {
        screen: MessagesScreen,
        navigationOptions: {
            title: "Messages"
        } },
}, {
    tabBarOptions: {
        activeTintColor: 'white',
        inactiveTintColor: "gray",
        style: {
            backgroundColor: "#8d8f8c",
            padding: 15,
        },
        tabStyle: {

        }
    },
});

export default createAppContainer(AppNavigator);
