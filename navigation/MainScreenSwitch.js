import { createSwitchNavigator } from 'react-navigation';

import MainCreateTask from "../src/components/MainCreateTask"
import MainTaskManager from "../src/components/MainTaskManager"


const TaskManagerNavigation = createSwitchNavigator({
  TaskManager: MainTaskManager,
  CreateTask: MainCreateTask
});

export default TaskManagerNavigation;
