import { createStackNavigator } from "react-navigation-stack";
import { createSwitchNavigator } from 'react-navigation';

import NotificationsScreen from '../src/screens/NotificationsScreen/NotificationsScreen';
import CommentsScreen from '../src/screens/CommentsScreen/CommentsScreen';


const _NotificationNavigator = createStackNavigator({
  Notifications: {
    screen: NotificationsScreen,
    navigationOptions: {
      title: 'Notifications'
    }
  }
});

const _CommentNavigator = createStackNavigator({
  Comments: {
    screen: CommentsScreen,
    navigationOptions: {
      title: 'Comments'
    }
  }
});

const NotificationNavigator = createSwitchNavigator({
  Notification: _NotificationNavigator,
  Comments: _CommentNavigator
});

export default NotificationNavigator
