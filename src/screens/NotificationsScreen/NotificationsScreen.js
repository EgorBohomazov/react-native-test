import React from 'react';
import { View, Text, StyleSheet, Button} from 'react-native'

const NotificationsScreen = ({navigation}) => {
  const switchToComment = () => {
    navigation.navigate('Comments')
  };

  return (
    <View style={styles.container}>
      <Text>Notifications</Text>
      <Button
        title="Comments"
        onPress={switchToComment}
      />
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  }
});

export default NotificationsScreen;
