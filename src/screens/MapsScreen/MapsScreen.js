import React, {useState} from 'react';
import { View, Text, StyleSheet, Dimensions} from 'react-native'
import MapView, {Marker} from 'react-native-maps'

const MapsScreen = () => {
  const [markers, setMarkers] = useState([]);

  const onMapPress =(e) => {
    setMarkers([...markers, {coordinate: e.nativeEvent.coordinate}])
  };


    return (
      <View style={styles.container}>
          <MapView
            style={styles.mapStyle}
            initialRegion={{
                latitude: 37.78825,
                longitude: -122.4324,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
            }}
            onPress={onMapPress}
          >
            {markers.map((marker, index) => (
              <Marker
                coordinate={marker.coordinate}
                key={index.toString()}
              />
            ))}
          </MapView>
      </View>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    mapStyle: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
    },
});

export default MapsScreen;
