import React, {useState, useEffect} from 'react';
import { StyleSheet, Text, View, ScrollView, Keyboard } from 'react-native';
import { Navbar } from '../../components/Navbar/Navbar';
import  TodoInput  from '../../components/TodoInput/TodoInput';
import  TodoItem  from "../../components/TodoItem/TodoItem"
import { connect } from "react-redux";
import * as Font from 'expo-font';

function TodoScreen(props) {


  useEffect(() => {
    Font.loadAsync({
      'fa-solid-900': require("../../../assets/fonts/fa-solid-900.ttf"),
    });
  }, []);

  const [list, setList] = useState([]);
  const { tasksList } = props;

  const add = (item) => {
    setList(prev => [...prev, item
    ]);
    Keyboard.dismiss()
  };

  const del = (id) => {
    setList(list.filter(todo => todo.id !== id))
  };

  return (
    <View style={styles.container}>
      <Navbar/>
      <TodoInput addFunc={add}/>
      <ScrollView style={styles.container}>
        {tasksList.map(i => {
          return(<TodoItem key={i.id} item={i} delFunc={del} />)
        })}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%"
  }
});

const mapStateToProps = state => {
  return {
    tasksList: state.taskReducer
  };
};


export default connect(mapStateToProps)(TodoScreen);
