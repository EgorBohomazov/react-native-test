import React from 'react';
import { View, Text, StyleSheet, Button} from 'react-native'

const CommentsScreen = ({navigation}) => {
  const switchToNotifications = () => {
    navigation.navigate('Notifications')
  };

  return (
    <View style={styles.container}>
      <Text>Comments</Text>
      <Button
        title="Notifications"
        onPress={switchToNotifications}
      />
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  }
});

export default CommentsScreen;
