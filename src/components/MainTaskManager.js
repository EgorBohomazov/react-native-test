import React, {useEffect} from 'react';
import { View, TextInput, StyleSheet, TouchableOpacity, ScrollView, StatusBar} from 'react-native'
import FontAwesome, { parseIconFromClassName } from "react-native-fontawesome";

const MainTaskManager = ({navigation}) => {
  const switchToCreateTask = () => {
    navigation.navigate('CreateTask')
  };
  const plusIcon = parseIconFromClassName("fa plus");
  const trashIcon = parseIconFromClassName("fas fa-trash");
  const checkIcon = parseIconFromClassName("fas fa-check");
  const penIcon = parseIconFromClassName("fas fa-pencil-alt");

  return (
    <View style={styles.container}>
      <View style={styles.managerHeader}>
        <TouchableOpacity onPress={switchToCreateTask}>
          <FontAwesome name="Add Task" style={{fontSize: 20}} icon={plusIcon}/>
        </TouchableOpacity>
        <TouchableOpacity onPress={switchToCreateTask}>
          <FontAwesome name="Add Task" style={{fontSize: 20}} icon={checkIcon}/>
        </TouchableOpacity>
        <TouchableOpacity onPress={switchToCreateTask}>
          <FontAwesome name="Add Task" style={{fontSize: 20}} icon={penIcon}/>
        </TouchableOpacity>
        <TouchableOpacity onPress={switchToCreateTask}>
          <FontAwesome name="Add Task" style={{fontSize: 20}} icon={trashIcon}/>
        </TouchableOpacity>
      </View>
      <ScrollView style={styles.tasksManageList}>

      </ScrollView>
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  managerHeader: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    padding: 20,
    backgroundColor: "#dee6dc",
  },
  tasksManageList: {
    height: "80%",
    backgroundColor: "#ebf5dc"
  }
});

export default MainTaskManager;
