import React, { useState } from 'react';
import { View, Text, StyleSheet, Button, TextInput, Dimensions } from 'react-native';
import MapView, { Marker } from 'react-native-maps';

const MainCreateTask = ({navigation}) => {
  const switchToTaskManager = () => {
    navigation.navigate( 'TaskManager' )
  };
  const [ title, setTitle ] = useState("");
  const [ description, setDescription ] = useState("");
  const [ location, setLocation ] = useState("");

  const changeTitle = (text) => {
    setTitle(text)
  };

  const changeDescription = (text) => {
    setDescription(text)
  };

  const createTask = () => {
    console.log({title, description, location})
  };

  const onMapPress = (e) => {
    setLocation(e.nativeEvent.coordinate)
  };

  const isMarker = () => {
    if(location !== ""){
      return (<Marker coordinate={location}/>)
    }
  };


  return (
    <View style={styles.container}>
      <View style={styles.header}>
      </View>
        <View style={styles.inputContainer}>
          <View style={styles.titleInputContainer}>
            <View><Text>Title</Text></View>
            <View><TextInput onChangeText={changeTitle} style={styles.titleInput}/></View>
          </View>
          <View style={styles.descriptionInputContainer} >
            <View><Text>Description</Text></View>
            <View><TextInput onChangeText={changeDescription} style={styles.descriptionInput} multiline={true}/></View>
          </View>
          <View style={styles.submitContainer}>
            <Button title="Create" onPress={createTask}/>
          </View>
        </View>
      <View style={styles.mapContainer}>
        <MapView style={styles.mapStyle} onPress={onMapPress}>
          {isMarker()}
        </MapView>
      </View>
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  inputContainer: {
    backgroundColor: "#ebf5dc",
    height: "50%",
    padding: 20
  },
  titleInputContainer: {
    height: "20%"
  },
  descriptionInputContainer: {
    marginTop: 10,
    height: "60%"
  },
  titleInput: {
    borderWidth: 1,
    borderColor: "#adadaa",
    borderStyle: "solid"
  },
  descriptionInput: {
    borderWidth: 1,
    borderColor: "#adadaa",
    borderStyle: "solid",
    height: "100%"

  },
  header: {

  },
  mapContainer: {
    height:"50%",
    borderWidth: 1,
    borderColor: "#adadaa",
    borderStyle: "solid",
  },
  mapStyle: {
    width: Dimensions.get('window').width,
    height: "100%"
  },
  submitContainer: {
    marginTop: 10
  }
});

export default MainCreateTask;
