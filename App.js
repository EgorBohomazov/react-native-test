import React, {useEffect, useState} from 'react';
import * as Font from 'expo-font';
import reducers from './reducers/index.js';
import { createStore } from 'redux';
import {Provider} from 'react-redux';
const store = createStore(reducers);
import { View, ActivityIndicator, StyleSheet } from 'react-native'
import AppNavigator from './navigation/AppNavigator'


export default function App() {
  const [isLoaded, setIsLoaded] = useState(false);

  useEffect(() => {
    Font.loadAsync({
      'fa-solid-900': require('./assets/fonts/fa-solid-900.ttf'),
    }).then(()=>{setIsLoaded(true)});
  }, []);


  const loadApp = () => {
    if(isLoaded === true){
      return (<AppNavigator/>)
    }
    else {
      return (
        <View style={styles.container}>
          <ActivityIndicator/>
        </View>
      )
    }
  };

  return(
    <Provider store={store}>
      {loadApp()}
    </Provider>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    width: "100%"
  }
});


