import {combineReducers} from 'redux';
import taskReducer from './taskReducer.js';

const reducers = combineReducers({taskReducer});

export default reducers;
