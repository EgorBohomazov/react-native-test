import { ADD_TASK, DELETE_TASK } from '../constants/actionTypes';

const initialState = [];


export default function(state=initialState, action){
  switch (action.type) {
    case ADD_TASK:
      return [...state, action.value];
    case DELETE_TASK:
      return state.filter(task => task.id === action.value)
  }
  return state;
}
